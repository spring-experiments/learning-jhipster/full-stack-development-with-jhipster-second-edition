import dayjs from 'dayjs';
import { IUser } from 'app/shared/model/user.model';

export interface IEmployee {
  id?: number;
  name?: string;
  age?: number | null;
  dob?: string | null;
  user?: IUser | null;
}

export const defaultValue: Readonly<IEmployee> = {};
