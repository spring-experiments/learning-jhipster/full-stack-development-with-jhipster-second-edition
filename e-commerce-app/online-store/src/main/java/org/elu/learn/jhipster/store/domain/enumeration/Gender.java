package org.elu.learn.jhipster.store.domain.enumeration;

/**
 * The Gender enumeration.
 */
public enum Gender {
    MALE,
    FEMALE,
    OTHER,
}
