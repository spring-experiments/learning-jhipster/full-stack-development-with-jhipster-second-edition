package org.elu.learn.jhipster.store.domain.enumeration;

/**
 * The OrderStatus enumeration.
 */
public enum OrderStatus {
    COMPLETED,
    PENDING,
    CANCELLED,
}
