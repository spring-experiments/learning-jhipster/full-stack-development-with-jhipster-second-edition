/**
 * Data Access Objects used by WebSocket services.
 */
package org.elu.learn.jhipster.store.web.websocket.dto;
