/**
 * View Models used by Spring MVC REST controllers.
 */
package org.elu.learn.jhipster.store.web.rest.vm;
