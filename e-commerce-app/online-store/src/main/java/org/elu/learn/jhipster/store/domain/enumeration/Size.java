package org.elu.learn.jhipster.store.domain.enumeration;

/**
 * The Size enumeration.
 */
public enum Size {
    S,
    M,
    L,
    XL,
    XXL,
}
