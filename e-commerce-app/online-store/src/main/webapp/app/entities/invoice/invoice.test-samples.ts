import dayjs from 'dayjs/esm';

import { InvoiceStatus } from 'app/entities/enumerations/invoice-status.model';
import { PaymentMethod } from 'app/entities/enumerations/payment-method.model';

import { IInvoice, NewInvoice } from './invoice.model';

export const sampleWithRequiredData: IInvoice = {
  id: 91509,
  date: dayjs('2023-10-30T01:22'),
  status: InvoiceStatus['ISSUED'],
  paymentMethod: PaymentMethod['CREDIT_CARD'],
  paymentDate: dayjs('2023-10-30T13:19'),
  paymentAmount: 24487,
  code: 'Expressway Berkshire',
};

export const sampleWithPartialData: IInvoice = {
  id: 32294,
  date: dayjs('2023-10-29T18:52'),
  details: 'Venezuela Arkansas auxiliary',
  status: InvoiceStatus['ISSUED'],
  paymentMethod: PaymentMethod['PAYPAL'],
  paymentDate: dayjs('2023-10-30T07:16'),
  paymentAmount: 28832,
  code: 'Buckinghamshire Florida Wooden',
};

export const sampleWithFullData: IInvoice = {
  id: 16008,
  date: dayjs('2023-10-29T22:14'),
  details: 'Chief Security',
  status: InvoiceStatus['PAID'],
  paymentMethod: PaymentMethod['CASH_ON_DELIVERY'],
  paymentDate: dayjs('2023-10-30T06:41'),
  paymentAmount: 88279,
  code: 'background',
};

export const sampleWithNewData: NewInvoice = {
  date: dayjs('2023-10-29T16:27'),
  status: InvoiceStatus['ISSUED'],
  paymentMethod: PaymentMethod['CREDIT_CARD'],
  paymentDate: dayjs('2023-10-30T02:43'),
  paymentAmount: 80001,
  code: 'Romania Borders',
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
